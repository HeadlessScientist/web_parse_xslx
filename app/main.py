import sys

from flask_apscheduler import APScheduler

from app import app, config
from app.subproccess import create_xlsx_table


if __name__=="__main__":
    scheduler = APScheduler()
    scheduler.add_job(
        func=create_xlsx_table,
        trigger="interval",
        id="create_xlsx_table",
        seconds=10,
        )
    scheduler.start()

    try:
        if sys.argv[1] == "debag":
            app.run(host=config.host, port=config.port, debug=True)
        else:
            print("Неверный аргумент")
    except IndexError:
        app.run(host=config.host, port=config.port, debug=False)