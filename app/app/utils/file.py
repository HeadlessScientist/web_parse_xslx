import os

from app import config

class XLSMFile:
    name: str
    path: str

    def __init__(self, name: str, path: str):
        self.name = name
        self.path = path


def get_xlsx() -> XLSMFile:
    """
        Метод получения всех табличных файлов из указанного хранилища
    """
    path = config.xlsx_storage
    for f in os.listdir(path):
        if not f.endswith('.xlsx'):
            continue
        yield XLSMFile(f, os.path.join(path, f))
