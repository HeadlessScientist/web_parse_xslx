from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate

from app.config import read_config


app = Flask(__name__)
config = read_config()
app.config['SQLALCHEMY_DATABASE_URI'] = config.database_uri
db = SQLAlchemy(app)
migrate = Migrate(app, db)


from app import views, models