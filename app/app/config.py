import os
import yaml

from dataclasses import dataclass

@dataclass
class Config:
    host: str
    port: int
    database_uri: str
    xlsx_storage: str


def read_config():
    basedir = os.getcwd()
    with open(f'app/static/config/config.yaml', 'r') as f:
        static = yaml.safe_load(f)
    return Config(
        host = static["host"],
        port = static["port"],
        database_uri = "sqlite:///" + basedir + static["database_uri"],
        xlsx_storage = basedir + static["xlsx_storage"],
    )
