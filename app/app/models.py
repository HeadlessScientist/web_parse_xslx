from datetime import datetime

from app import db


class History(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    table_name = db.Column(db.String, unique=True)
    date = db.Column(db.DateTime, default=datetime.utcnow)
    