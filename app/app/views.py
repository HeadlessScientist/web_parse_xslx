import os

from pandas import read_excel, read_sql_table
from flask import render_template, make_response

from app import app, db, models
from app.utils.file import get_xlsx


@app.route('/')
def main_page():
    """
        Запрос для вызова главной страницы и отображения таблицы истории
    """
    history = models.History.query.all()
    return render_template("index.html", history=history)


@app.route('/show_table/<tablename>')
def show_table(tablename: str):
    """
        Запрос для отображения таблицы
    """
    with db.session() as session:
        table = read_sql_table(tablename, session.connection())
    return table.to_html()


@app.get('/create_table')
def create_table():
    """
        Запрос для создания новой таблицы
    """
    with db.session() as session:
        for xlsm in get_xlsx():
            table = read_excel(xlsm.path)
            try:
                table.to_sql(xlsm.name, session.connection())
                session.add(models.History(table_name=xlsm.name))
                os.remove(xlsm.path)
            except Exception as ext:
                print(ext)
                make_response(str(ext), 500)
            session.commit()
        return make_response("Ok", 200)
